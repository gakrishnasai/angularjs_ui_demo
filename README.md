# AcitivitystreamUi - A Chat App with JAVA Spring Boot backend 

This project only contains the font end.

## Functionality-> 
    1. Sign In.
    2. Sign Up.
    3. Create a Circle/Group.
    4. Subscribe or unsubscibe to the circle.
    5. Send message to user.
    6. Send message to circl.

## Technology Stack->
    1. Angular 5
    2. HTLM5, CSS3, Fontawesome
    3. Angular Material

## Future Scope->
    1. Realtime chat using Spring Websocket will be implemented.

## Know Issue->
    1. Application is not much responsive
    

