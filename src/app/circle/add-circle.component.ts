import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-add-circle',
  templateUrl: './add-circle.component.html',
  styleUrls: ['./add-circle.component.css']
})
export class AddCircleComponent implements OnInit {

  ngOnInit() {
  }
  constructor(
    public dialogRef: MatDialogRef<AddCircleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    console.log("I am in onNoClick()")
    this.dialogRef.close();
  }
}
