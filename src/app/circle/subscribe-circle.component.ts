import { SharedService } from './../services/shared.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-subscribe-circle',
  templateUrl: './subscribe-circle.component.html',
  styleUrls: ['./subscribe-circle.component.css']
})
export class SubscribeCircleComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<SubscribeCircleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private sharedService: SharedService) {

  }

  circles: { value: string, viewValue: string }[] = [];
  selectedCircle: string;
  allCircles = [];

  ngOnInit() {
    this.sharedService.getCircles().subscribe(
      data => {
        console.log(data['circleName']);
        for (var i = 0; i < data.length; i++) {
            this.allCircles.push(data[i]['circleName']);
        }
        var subscribedCircles = this.data['subscribedCircles'];
        var allCircles = this.allCircles;
        var avaiableCircle = 
          allCircles.filter(function (obj) { return subscribedCircles.indexOf(obj) == -1; });
        for(var i = 0; i< avaiableCircle.length ; i++){
          this.circles.push({
            value: (avaiableCircle[i]),
            viewValue: (avaiableCircle[i])
          })
        }
        // console.log(this.allCircles);
        // console.log(avaiableCircle);
        // console.log(this.circles);
      }
    );


  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
