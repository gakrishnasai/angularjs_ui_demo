import { AddCircleComponent } from './add-circle.component';
import { SubscribeCircleComponent } from './subscribe-circle.component'
import { Component, EventEmitter, OnInit, Inject } from '@angular/core'
import { SharedService } from '../services/shared.service'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { isUndefined } from 'util';

@Component({
    selector: 'app-circle',
    templateUrl: './circle.component.html',
    styleUrls: ['./circle.component.css']
})

export class Circle implements OnInit {
    // circles = ['Circle1','Circle2','Circle3'];
    circles = [];
    constructor(private sharedService: SharedService, public dialog: MatDialog) {
    }

    ngOnInit() {
        this.sharedService.getSubscribedCircles().subscribe(
            data => {
                for (var i = 0; i < data.length; i++) {
                    this.circles.push(data[i]);
                }
            }
        );
    }

    newCircleName: string = '';
    subscribeCircleName: string = '';

    // openSnackBar(message: string, action: string) {
    //     this.snackBar.open(message, action, {
    //       duration: 2000,
    //     });
    //   }


    openAddCircleDialog(): void {
        let dialogRef = this.dialog.open(AddCircleComponent, {
            width: '250px',
            data: { circleName: this.newCircleName}
        });

        dialogRef.afterClosed().subscribe(result => {
            if(!isUndefined(result)){
                console.log('I am here! in if! how come?')
                this.sharedService.createNewCircle(result)
                    .subscribe(
                        response => {
                            if (response.status == 201) {
                                this.sharedService.subscribeNewCircle(result).subscribe();
                                this.sharedService.openSnackBar("Circle created successfully", "Close");
                                this.circles.push(response.json().circleName);
                            }
                        },
                        error => {
                            console.log(error);
                            if(error.status == 409)
                                this.sharedService.openSnackBar("Circle already present!","Close");
                            else
                                this.sharedService.openSnackBar("Error Occured!","Close");
                        }
                    );
            }
        });
    }

    openSubscribeCircleDialog(): void {
        let dialogRef = this.dialog.open(SubscribeCircleComponent, {
            width: '250px',
            data: { circleName: this.subscribeCircleName, subscribedCircles: this.circles  }
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed: subscribe');
            console.log(result);
            if(!isUndefined(result)){
            this.sharedService.subscribeNewCircle(result)
                .subscribe(
                    response => {

                        if (response.status == 200) {
                            console.log(response);
                            this.sharedService.openSnackBar("Circle subscribed successfully", "Close");
                            console.log("subscribeCircleName " + result);
                            this.circles.push(result);
                        }
                    },
                    error => {
                        console.log(error);
                        if(error.status == 500)
                            this.sharedService.openSnackBar("Circle already subscribed!","Close");
                    }
                );
            }
        });

    }

    // makeBold(receiverCircle){
    //     this.sharedService.storeReciverName.emit(receiverCircle);
    // }
    getMessage(circle: string) {
        const values = {
            value: circle,
            type: 'circle'
        }
        this.sharedService.messageEmitter.emit(values);
    }

    //@PutMapping("/removeFromCircle/{username}/{circleName}")
    unSubscribeCircle(circle) {

        this.sharedService.unSubscribeCircle(circle)
            .subscribe(
                response => {
                    console.log(response)
                    if (response.status == 200) {
                        console.log("unSubscribeCircle: " + circle);
                        let index = this.circles.indexOf(circle);
                        console.log(index);
                        if (index > -1) {
                            this.circles.splice(index, 1);
                        }
                        this.sharedService.openSnackBar("Circle unsubscribed successfully", "Close");
                    }
                }
            );
    }
}