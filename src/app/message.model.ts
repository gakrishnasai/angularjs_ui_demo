export class MessageType {

    senderName: string;
    receiverName: string;
    message: string;

    constructor(senderName: string, receiverName: string, message: string)
      {
      this.senderName = senderName;
      this.receiverName = receiverName;
      this.message = message;
    }
  }