import { Component, EventEmitter, OnInit } from '@angular/core'
import { SharedService } from '../services/shared.service'


@Component({
    selector:'app-user',
    templateUrl: './user.component.html',
    styleUrls:['./user.component.css']
})

export class User implements OnInit{
    // usersList = ['Amit','Suresh','Rahul'];
    usersList = []
    constructor(private sharedService:SharedService){
    }

    ngOnInit(){
        this.sharedService.getUsers().subscribe(
            data =>
            {
                for(var i = 0; i < data.length;i++){
                    this.usersList.push(data[i]['username']);
                }
            }
        );
    }
    
    getMessage(user:string){
        const values = {
            value: user,
            type: 'user'
        }
        this.sharedService.messageEmitter.emit(values);
        // this.messageEmitter.emit(this.messageBody);
    }

    
}