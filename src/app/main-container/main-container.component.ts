import { AuthService } from './../auth/auth.service';
import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-container',
  templateUrl: './main-container.component.html',
  styleUrls: ['./main-container.component.css']
})
export class MainContainerComponent implements OnInit {

  loggedInUser:string;
  constructor(private authService:AuthService, private router:Router) {
    
   }
  
  ngOnInit() {
    if(sessionStorage.getItem('token') == null){
      this.router.navigateByUrl('signin');
    }
    if(sessionStorage.getItem('userName') != null){
      this.loggedInUser = sessionStorage.getItem('userName');
    }
  }
  logOutUser(){
    sessionStorage.clear();
  }
}
