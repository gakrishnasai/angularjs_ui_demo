import { Message } from './../../message/message.component';
import { SharedService } from './../../services/shared.service';
import { AuthService } from './../auth.service';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  constructor(private authService: AuthService, private router:Router
    , private sharedService:SharedService){

    }

  ngOnInit() {
  }

  TOKEN:string = null;
  onSignin(form: NgForm){
    console.log(form);
    const cred = {
      username:form.value.username,
      password:form.value.password
    }
    this.authService.signInUser(cred)
      .subscribe(
        (data) => {
          if(data.status == 200){
            this.router.navigateByUrl('/stackchat');
            sessionStorage.setItem('token', data.json().token);
            sessionStorage.setItem('userName', form.value.username);
            this.sharedService.openSnackBar("Signed in sucessfully!","Close");
          }
        }, err => {
          console.log(err);
          this.sharedService.openSnackBar(err.json()['message'],"Close");
        }
      );
  }
}
