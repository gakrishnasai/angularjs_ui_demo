import { Http, HttpModule, Response, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { SharedService } from '../services/shared.service';

@Injectable()
export class AuthService {

  token: string = '';
  constructor(private http:Http, private router:Router,
    private sharedService:SharedService) { }
  
  BASE_SIGN_IN_URL = 'http://localhost:8082/login';
  BASE_SIGN_UP_URL = 'http://localhost:8082/createUser';

  signUpUser(user: {username: string, name: string, password: string}) {
    const headers = new Headers({
      'Content-Type': 'Application/json'
    });
    this.http.post(this.BASE_SIGN_UP_URL, user,
      {headers: headers})
      .subscribe(
        (data) => {
          if(data.status == 201){
            this.router.navigateByUrl('/signin');
          }
        },err => {
          console.log(err.status);
          // console.log(err.json());
          if(err.status == 409){
            this.sharedService.openSnackBar("User alredy exists!","Close");
          }
        }
      )
  }

  
  
  signInUser(cred:{username:string, password: string}) {
    const headers = new Headers({
      'Content-Type': 'Application/json'
    });
    return this.http.post(this.BASE_SIGN_IN_URL, cred,
      { headers: headers })
      .map(
        (data) => {
          return data;
        }
      )
  }

  
}
