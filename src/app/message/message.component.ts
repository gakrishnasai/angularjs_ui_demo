import { AuthService } from './../auth/auth.service';
import { Component, OnInit } from '@angular/core'
import { SharedService } from '../services/shared.service'
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
@Component({
    selector: 'app-message',
    templateUrl: './message.component.html',
    styleUrls: ['./message.component.css']
})


export class Message implements OnInit{

    private serverUrl = 'http://localhost:8086/stackchat';
    private stompClient;

    receiverName: string = 'Suresh';
    messageLists = [];
    messageBody: { senderName: string, receiverName: string, message };
    message: string = '';
    senderName: string = '';
    messageContentToShow = [];
    LOGGEDIN_USER = sessionStorage.getItem('userName');
    type = ''

    ngOnInit(){}
    constructor(private sharedService: SharedService, private authService: AuthService) {
        this.initializeWebSocketConnection();
        this.sharedService.messageEmitter.subscribe(
            data => {
                if(data.type == "circle"){
                    this.type = data.type;
                    console.log('I am in circle');
                    this.getMessage(data.value);
                }
                else if (data.type == "user"){
                    console.log('I am in user');
                    this.type = data.type;
                    this.getUserMessage(data.value);
                }
            }
        );
        this.sharedService.storeReciverName.subscribe(
            (data) => {
            this.receiverName = data;
                // console.log(this.receiverName);
            }
        );
        
    }

    initializeWebSocketConnection(){
        let ws = new SockJS(this.serverUrl);
        this.stompClient = Stomp.over(ws);
        let that = this;
        this.stompClient.connect({}, function(frame) {
          that.stompClient.subscribe("/chat", (message) => {
            if(message.body) {
              console.log("/chat");
              console.log(message.body);
            }
          });
          that.stompClient.subscribe("/privateRoom", (message) => {
            if(message.body) {
              console.log("/privateRoom");
              console.log(message.body);
            }
          });
        });
      }

    //Executes on clicking the message Body
    onSend() {

        
        // this.messageContentToShow.push(
        //     {
        //         message: this.message,
        //         senderName: sessionStorage.getItem('userName'),
        //         postedDate: Date.now()
        //     }
        // )
        this.messageLists.push(this.message);
        this.sharedService.storeReciverName.subscribe(
            (data) => this.receiverName = data
        );
        this.messageBody = {
            senderName: sessionStorage.getItem('userName'),
            receiverName: this.receiverName,
            message: this.message
        }
        console.log(this.type);
        if(this.type == 'circle'){
            console.log('I am in circle - saving');
            this.stompClient.send("stackchat/privateRoom/" + this.receiverName , {}, this.message);
            this.sharedService.saveMessage(this.messageBody).subscribe(
                response => {
                    if(response.status == 200 ){
                        this.getMessage(this.receiverName);
                    }
                }
            );
        }
        else if(this.type == 'user'){
            console.log('I am in user - saving');
            this.stompClient.send("/stackchat/send/message" , {}, this.message);
            this.sharedService.saveUserMessage(this.messageBody).subscribe(
                response => {
                    if(response.status == 200){
                        this.getUserMessage(this.receiverName);
                    }
                }
            );
        }
        
        this.message = '';
        document.getElementById("txtMessage").focus();
    }

    getUserMessage(value: string){
        this.sharedService.getUserMessage(value).subscribe(
            response => {
                this.messageContentToShow = [];
                for (let l = 0; l < response.length; l++) {
                    this.messageContentToShow.push(
                        {
                            messageId: response[l]['messageId'],
                            message: response[l]['message'],
                            senderName: response[l]['senderName'],
                            postedDate: response[l]['postedDate']
                        }
                    )
                }
            }, error => {
                console.log("Error Occured");
            }
        )
    }

    getMessage(value: string){
        this.sharedService.getMessage(value).subscribe(
            response => {
                this.messageContentToShow = [];
                for (let l = 0; l < response.length; l++) {
                    this.messageContentToShow.push(
                        {
                            messageId: response[l]['messageId'],
                            message: response[l]['message'],
                            senderName: response[l]['senderName'],
                            postedDate: response[l]['postedDate']
                        }
                    )
                }
            }, error => {
                console.log("Error Occured");
            }
        )
    }
}