import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MaretialModule } from './shared/maretial.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {Routes, RouterModule} from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { Message } from './message/message.component';
import { User } from './user/user.component';
import { Circle } from './circle/circle.component';

import { SharedService } from './services/shared.service';
import { MainContainerComponent } from './main-container/main-container.component';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';
import { AuthService } from './auth/auth.service';
import { AddCircleComponent } from './circle/add-circle.component';

import { SubscribeCircleComponent } from './circle/subscribe-circle.component';


const routes:Routes = [
  // {path:'', component:AuthenticationComponent },
  {path:'stackchat', component:MainContainerComponent },
  {path:'signup', component: SignupComponent},
  {path:'signin', component: SigninComponent},
  {path:'**', redirectTo: 'stackchat'}
]


@NgModule({
  declarations: [
    AppComponent,
    Message,
    User,
    Circle,
    MainContainerComponent,
    SigninComponent,
    SignupComponent,
    AddCircleComponent,
    SubscribeCircleComponent
  ],
  imports: [
    BrowserModule,
    MaretialModule,
    FormsModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    HttpModule
  ],
  entryComponents: [Circle, AddCircleComponent, SubscribeCircleComponent],
  providers: [SharedService,AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
