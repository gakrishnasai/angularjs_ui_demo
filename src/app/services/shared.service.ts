import { MatSnackBar } from '@angular/material';
import { Message } from './../message/message.component';
import { EventEmitter, Injectable, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable'
import { MessageType } from '../message.model';
import { HttpClient } from '@angular/common/http';
import { storage } from 'firebase';

@Injectable()
export class SharedService implements OnInit{

    constructor(private http: Http, public snackBar: MatSnackBar) { }

    ngOnInit(){
        
    }
    messageEmitter = new EventEmitter<{ value: string, type: string }>();
    storeReciverName = new EventEmitter<String>();

    userOrCircleName: string;
    messageBody = new EventEmitter<object>();
    messages: object;
    token: string = sessionStorage.getItem('token');
    LOGGEDIN_USER: string = sessionStorage.getItem('userName');
    BASE_CIRCLE_URL: string = 'http://localhost:8084/api/circle';
    BASE_USER_URL: string = 'http://localhost:8082/api/user';
    BASE_MSG_URL: string = 'http://localhost:8086/api/message';
    BASE_USR_CRCL_URL: string = 'http://localhost:8085/api/usercircle';

    header = new Headers({
        'Content-Type': 'Application/json',
        'Authorization': 'Bearer ' + this.token
    });

    
    setReciverName(reciverName: string) {
        console.log(reciverName);
    }

    makeBold(userOrCircle: string) {
        this.storeReciverName.emit(userOrCircle);
        this.userOrCircleName = userOrCircle;
    }

    saveMessage(messageBody: { senderName: string, receiverName: string, message: string }) {
        let url = this.BASE_MSG_URL + '/sendMessageToCircle/' + messageBody.receiverName;
        const msg = {
            senderName: messageBody.senderName,
            circleName: messageBody.receiverName,
            message: messageBody.message
        }
        return this.http.post(url, msg,
            { headers: this.header })
            .map(
                response => {
                    console.log(response);
                    return response;
                })
    }

    //  /sendMessageToUser/{receiverId}
    saveUserMessage(messageBody: { senderName: string, receiverName: string, message: string }) {
        let url = this.BASE_MSG_URL + '/sendMessageToUser/' + messageBody.receiverName;
        const msg = {
            senderName: messageBody.senderName,
            circleName: messageBody.receiverName,
            message: messageBody.message
        }
        return this.http.post(url, msg,
            { headers: this.header })
            .map(
                response => {
                    console.log(response);
                    return response;
                })
    }

    // --------------------------Get Messages by User------------------------------------/
    // @GetMapping("/getMessagesByUser/{senderUsername}/{receiverUserName}/{pageNumber}")

    getMessage(userOrCircle: string): Observable<any> {
        this.userOrCircleName = userOrCircle;
        this.storeReciverName.emit(userOrCircle);
        if (sessionStorage.getItem('userName') != null || sessionStorage.getItem('userName') == '') {
            let url = this.BASE_MSG_URL + '/getMessagesByCircle/' + userOrCircle + '/1';
            return this.http.get(url,
                { headers: this.header }).map(
                    response => {
                        console.log(response.json());
                        return response.json();
                    }
                ).catch(error => { return Observable.throw(error.message) }
                );
        }
    }

    // -----------------------Get Messages by User---------------------------------------/
    //@GetMapping("/getMessagesByUser/{senderUsername}/{receiverUserName}/{pageNumber}")
    //http://localhost:8086/api/message/getMessagesByUser/Amit/Rahul/1
    getUserMessage(user: string) {
        this.userOrCircleName = user;
        this.storeReciverName.emit(user);
        if (sessionStorage.getItem('userName') != null || sessionStorage.getItem('userName') == '') {
            let url = this.BASE_MSG_URL + '/getMessagesByUser/' + sessionStorage.getItem('userName') + '/' + user + '/1';
            console.log(url);
            return this.http.get(url,
                { headers: this.header }
            ).map(
                response => {
                    return response.json();
                }
            ).catch(error => { return Observable.throw(error.message) }
            )
        }
    }

    // {
    //     "circleName": "TestCircle",
    //     "creatorId": "Amit",
    //     "createdDate": 1520447448000
    // }
    createNewCircle(result: string) {
        const value = {
            "circleName": result,
            "creatorId": sessionStorage.getItem('userName')
        }
        return this.http.post(
            this.BASE_CIRCLE_URL, value,
            { headers: this.header }
        ).map(
            response => {
                return response;
            },
            error => console.log(error)
        )
    }

    // @PutMapping("/addToCircle/{username}/{circleName}")
    subscribeNewCircle(result: string) {
        const value = {
            "circleName": result,
            "creatorId": sessionStorage.getItem('userName')
        }
        // this.header.set('Access-Control-Allow-Origin','http://localhost:4200');
        let url = this.BASE_USR_CRCL_URL + '/addToCircle/' + sessionStorage.getItem('userName') + '/' + result;
        return this.http.put(
            url, value,
            { headers: this.header }
        ).map(
            response => {
                return response;
            },
            error => console.log(error)
        )
    }

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
          duration: 2000,
        });
      }
    
    unSubscribeCircle(circle:string){
        const value = {
            "circleName": circle,
            "username": sessionStorage.getItem('userName')
        }
        // this.header.set('Access-Control-Allow-Origin','http://localhost:4200');
        let url = this.BASE_USR_CRCL_URL + '/removeFromCircle/' + sessionStorage.getItem('userName') + '/' + circle;
        return this.http.put(
            url, value,
            { headers: this.header }
        ).map(
            response => {
                return response;
            },
            error => console.log(error)
        )
    }

    getCircles() {
        return this.http.get(this.BASE_CIRCLE_URL,
            { headers: this.header })
            .map(
                (response) => {
                    console.log(response.json());
                    return response.json();
                },
                (error) => console.log(error)
            )
    }

    getSubscribedCircles() {
        let url = this.BASE_USR_CRCL_URL + '/searchByUser/' + sessionStorage.getItem('userName'); 
        return this.http.get(url,
            { headers: this.header })
            .map(
                (response) => {
                    console.log(response.json());
                    return response.json();
                },
                (error) => console.log(error)
            )
    }

    getUsers() {
        return this.http.get(this.BASE_USER_URL,
            { headers: this.header })
            .map(
                (response) => {
                    console.log(response);
                    return response.json();
                },
                (error) => console.log(error)
            )
    }
}