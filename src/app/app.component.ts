import { Component, OnInit } from '@angular/core';
import { SharedService } from './services/shared.service'

import * as firebase from 'firebase';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  testString: string = 'Testing';
  constructor(private sharedService:SharedService){
  }

  ngOnInit(){
    firebase.initializeApp({
      apiKey: "AIzaSyDok0lUuf-tlTZ369nQtDDX0bnrB5dAEyo",
      authDomain: "ngchartapp.firebaseapp.com"
    });
  }

}
